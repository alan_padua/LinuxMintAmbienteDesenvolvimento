
# Ambiente de Desenvolvimento

## Instalar o Docker

Atualizar a lista de repositórios
**Comando:**

	sudo apt-get update

Instalando as dependências 
**Comando:**

	sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

Baixando e instalando a chave do repositório
**Comando:**

	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

	sudo apt-key fingerprint 0EBFCD88

	sudo add-apt-repository \
	   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
	   $(lsb_release -cs) \
	   stable"

Instalando o docker: **docker-ce**, **docker-ce-cli** e **containerd.io**.
**Comando:**
	
	sudo apt-get update

	sudo apt-get install docker-ce docker-ce-cli containerd.io

Verificando a instalação
**Comando:**

	docker --version

## Usar o docker sem sudo

Criando o grupo do docker
**Obs:** Se sair a mensagem que o grupo ja existe, pode ir para o proximo comando
**Comando:**

	sudo groupadd docker

Adicionando o grupo do docker ao root
**Comando:**

	sudo usermod -aG docker $USER


## Instalando o GIT

Instalando o git, gerenciado de versões 
**Comando:** 

	sudo apt install git-all

Verificando a instalação:
**Comando:**

	git --version

## Instalação portainer

Somente é possível instalar se o **Docker** estiver instalado.

Baixar o portainer pelo Docker
**Comando:**

	docker pull portainer/portainer

Executar o portainer como container 
**Comando:**
	
	docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer

**URL local:** http://localhost:9000/#/containers

Deve definir o **usuário** e **senha** na primeira execução, **É bom anotar**


## Instalar o JDK Java

Instalando duas versões mais utilizadas pelos tutoriais
Instalando o openjdk 8 e 14.
**Comando:**

	sudo apt install openjdk-8-jdk
	sudo apt install openjdk-14-jdk

Alternando as versões do java
**Comando:**

	sudo update-alternatives --config java

## Instalação do Maven

Instalar o Maven, gerenciador de dependências do Java
**Comando:** 
	
	sudo apt install maven

Verificando a instalação
**Comando:**

	mvn --version
	
## Instalação do NVM

Install NVM, gerenciador de versões do Node.js
**Comando:** 
	
	sudo apt update
	sudo apt install build-essential -y

	curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

Atualizando o **.bashrc** para refletir a instalação
**Comando:**

	source .bashrc
	
Verificando a instalação
**Comando:**
	
	nvm --version
	
## Instalando o Node.js pelo NVM


Verificando as versões disponíveis, escolha a versão que for utilizar
**Comando:**

	nvm ls-remote node

Instalando a versão **v15.8.0** que é a mais atual
**Comando:**

	nvm install v15.8.0


Verificando da versão instalada
**Comando:**

	node -v

Lista as versões instaladas
**Comando:**

	nvm ls

Alterando as versões
**Comando:**
	
	nvm use $VERSAO_DA_LISTA

## Instalando o DBeaver
Baixar o arquivo .deb

Ur: https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb

Pode ser instalado com duplo click em cima.

Caso queira instalar por linha de comando

**Comando:**

	sudo dpkg -i dbeaver-ce_latest_amd64.deb

## Instalando o Eclipse

Baixar o eclipse
**Url:** https://eclipse.c3sl.ufpr.br/oomph/epp/2020-12/R/eclipse-inst-jre-linux64.tar.gz

